package br.ucsal.atividade2.poo.business;

import java.util.List;
import java.util.Set;

import br.ucsal.atividade2.poo.domain.Caminhao;
import br.ucsal.atividade2.poo.domain.Onibus;
import br.ucsal.atividade2.poo.domain.Veiculo;
import br.ucsal.atividade2.poo.exception.ExistingPlateException;
import br.ucsal.atividade2.poo.exception.InvalidValueException;
import br.ucsal.atividade2.poo.exception.ListEmptyException;
import br.ucsal.atividade2.poo.persistence.LocadoraDAO;

public class LocadoraBO {

	private LocadoraDAO locadoraDao = new LocadoraDAO();

	public void cadastrarOnibus(Onibus onibus) throws InvalidValueException, ExistingPlateException {
		validar(onibus.getValor());
		validar(onibus);
		locadoraDao.cadastrarOnibus(onibus);
	}

	public void cadastrarCaminhao(Caminhao caminhao) throws InvalidValueException, ExistingPlateException {
		validar(caminhao.getValor());
		validar(caminhao);
		locadoraDao.cadastrarCaminhao(caminhao);
	}

	public List<Veiculo> listarVeiculosOrdenadoPorValor() throws ListEmptyException {
		validar(locadoraDao.listarVeiculosOrdenadoPorValor());
		return locadoraDao.listarVeiculosOrdenadoPorValor();
	}

	public List<Veiculo> listarVeiculosOrdenadoPorPlaca() throws ListEmptyException {
		validar(locadoraDao.listarVeiculosOrdenadoPorPlaca());
		return locadoraDao.listarVeiculosOrdenadoPorPlaca();
	}

	public Set<String> listarModelo() throws ListEmptyException {
		validar(locadoraDao.listarModelos());
		return locadoraDao.listarModelos();
	}

	private void validar(Set<String> list) throws ListEmptyException {// Tentativa de utilizar uma exception pra caso a
																		// lista esteja vazia
		if (locadoraDao.listarModelos().isEmpty()) {
			throw new ListEmptyException("Nenhum modelo cadastrado");
		}
	}

	private void validar(List<Veiculo> list) throws ListEmptyException {// Tentativa de utilizar uma exception pra caso
																		// a lista esteja vazia
		if (locadoraDao.listarVeiculosOrdenadoPorValor().isEmpty()
				|| locadoraDao.listarVeiculosOrdenadoPorPlaca().isEmpty()) {
			throw new ListEmptyException("Nenhum ve�culo cadastrado");
		}
	}

	private void validar(Veiculo veiculo) throws ExistingPlateException {
		if (locadoraDao.validate(veiculo) == false) {
			throw new ExistingPlateException("Est� placa j� existe");
		}
	}

	private void validar(Double valor) throws InvalidValueException {
		if (valor <= 0) {
			throw new InvalidValueException("Valor n�o v�lido");
		}
	}
}
