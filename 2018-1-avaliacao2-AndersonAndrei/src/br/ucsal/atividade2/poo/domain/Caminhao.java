package br.ucsal.atividade2.poo.domain;

public class Caminhao extends Veiculo {
	private Integer qtdEixos;
	private Integer capCarga;

	public Caminhao(String placa, String modelo, Integer anoFabricacao, Double valor, Integer qtdEixos,
			Integer capCarga) {
		super(placa, modelo, anoFabricacao, valor);
		this.qtdEixos = qtdEixos;
		this.capCarga = capCarga;
	}

	@Override
	public Double custoLocacao(Integer capCarga) {
		Double custo = 0d;
		
		custo = capCarga * 8d;
		
		return custo;
	}
	
	public Integer getQtdEixos() {
		return qtdEixos;
	}

	public void setQtdEixos(Integer qtdEixos) {
		this.qtdEixos = qtdEixos;
	}

	public Integer getCapCarga() {
		return capCarga;
	}

	public void setCapCarga(Integer capCarga) {
		this.capCarga = capCarga;
	}

	@Override
	public String toString() {
		return "Caminhao " + super.toString()+ "qtdEixos="  + qtdEixos + ", capCarga=" + capCarga +  "]";
	}

}
