package br.ucsal.atividade2.poo.domain;

public class Onibus extends Veiculo{
	private Integer qtdMax;

	public Onibus(String placa, String modelo, Integer anoFabricacao, Double valor, Integer qtdMax) {
		super(placa, modelo, anoFabricacao, valor);
		this.qtdMax = qtdMax;
	}
	
	@Override
	public Double custoLocacao(Integer qtdMax) {
		Double custo = 0d;
		
		custo = qtdMax * 10d;
		
		return custo;
	}
	
	public Integer getQtdMax() {
		return qtdMax;
	}

	public void setQtdMax(Integer qtdMax) {
		this.qtdMax = qtdMax;
	}

	@Override
	public String toString() {
		return "Onibus " + super.toString() + "qtdMax=" + qtdMax +  "]";
	}
	
	
}
