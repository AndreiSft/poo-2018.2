package br.ucsal.atividade2.poo.domain;

public abstract class Veiculo implements Comparable<Veiculo> {
	private String placa;
	private String modelo;
	private Integer anoFabricacao;
	private Double valor;

	public Veiculo(String placa, String modelo, Integer anoFabricacao, Double valor) {

		this.placa = placa;
		this.modelo = modelo;
		this.anoFabricacao = anoFabricacao;
		this.valor = valor;
	}
	
	public abstract Double custoLocacao(Integer qtdMax);
	
	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public Integer getAnoFabricacao() {
		return anoFabricacao;
	}

	public void setAnoFabricacao(Integer anoFabricacao) {
		this.anoFabricacao = anoFabricacao;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	public int compareTo(Veiculo veiculo) {
		return this.placa.compareToIgnoreCase(veiculo.placa);
	}

	@Override
	public String toString() {
		return "[placa=" + placa + ", modelo=" + modelo + ", anoFabricacao=" + anoFabricacao + ", valor=" + valor
				+ ", ";
	}

}
