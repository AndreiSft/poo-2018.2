package br.ucsal.atividade2.poo.persistence;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import br.ucsal.atividade2.poo.domain.Caminhao;
import br.ucsal.atividade2.poo.domain.Onibus;
import br.ucsal.atividade2.poo.domain.Veiculo;
import br.ucsal.atividade2.poo.utility.OrdenarPorPlaca;
import br.ucsal.atividade2.poo.utility.OrdenarPorValor;

public class LocadoraDAO {
	private static List<Veiculo> veiculos = new ArrayList<>();
	
	public void cadastrarOnibus(Onibus onibus) {
		veiculos.add(onibus);
	}

	public void cadastrarCaminhao(Caminhao caminhao) {
		veiculos.add(caminhao);
	}

	public List<Veiculo> listarVeiculosOrdenadoPorValor() {
		Collections.sort(veiculos, new OrdenarPorValor());
		return veiculos;
	}

	public List<Veiculo> listarVeiculosOrdenadoPorPlaca() {
		Collections.sort(veiculos, new OrdenarPorPlaca());
		return veiculos;
	}

	public Set<String> listarModelos() {
		Set<String> modelos = new HashSet<>();
		for (Veiculo veiculo : veiculos) {
			modelos.add(veiculo.getModelo());
		}
		return modelos;
	}
	
	public boolean validate(Veiculo veiculoUsuario) {
		for (Veiculo veiculo : veiculos) {
			if (veiculoUsuario.getPlaca().equalsIgnoreCase(veiculo.getPlaca())) {
				return false;
			}
		}
		return true;
	}
}
