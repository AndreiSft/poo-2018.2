package br.ucsal.atividade2.poo.utility;

import java.util.Scanner;

public class UtilityMethods {

	private Scanner sc = new Scanner(System.in);

	public String getString(String txt) {
		while (true) {
			System.out.println(txt);
			String string = sc.nextLine();
			try {
				validate(string);
				return string;
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
		}
	}

	public Integer getInteger(String txt) {
		while (true) {
			System.out.println(txt);
			try {
				Integer number = sc.nextInt();
				sc.nextLine();
				validate(number);
				return number;
			} catch (java.util.InputMismatchException e) {
				sc.nextLine();
				System.out.println("Only numbers are accepted, please try again");
			}  catch (Exception ex) {
				System.out.println(ex.getMessage());
			}
		}
	}
	
	public Double getDouble(String txt) {
		while (true) {
			System.out.println(txt);
			try {
				Double number = sc.nextDouble();
				sc.nextLine();
				return number;
			} catch (java.util.InputMismatchException e) {
				sc.nextLine();
				System.out.println("Only numbers are accepted, please try again");
			}
		}
	}
	
	private void validate(Integer number) throws Exception {
		String string = Integer.toString(number);
		if (string.trim().isEmpty()) {
			throw new Exception("This field is required. Please fill out !");
		}
	}

	private void validate(String string) throws Exception {
		if (string.trim().isEmpty()) {
			throw new Exception("This field is required. Please fill out !");
		}
	}
}
