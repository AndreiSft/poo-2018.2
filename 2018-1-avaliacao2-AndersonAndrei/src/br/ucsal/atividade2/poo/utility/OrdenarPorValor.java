package br.ucsal.atividade2.poo.utility;

import java.util.Comparator;

import br.ucsal.atividade2.poo.domain.Veiculo;

public class OrdenarPorValor implements Comparator<Veiculo> {

	@Override
	public int compare(Veiculo veiculo1, Veiculo veiculo2) {
		return veiculo1.getValor().compareTo(veiculo2.getValor());
	}

}
