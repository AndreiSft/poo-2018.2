package br.ucsal.atividade2.poo.tui;

import java.util.List;
import java.util.Set;

import br.ucsal.atividade2.poo.business.LocadoraBO;
import br.ucsal.atividade2.poo.domain.Caminhao;
import br.ucsal.atividade2.poo.domain.Onibus;
import br.ucsal.atividade2.poo.domain.Veiculo;
import br.ucsal.atividade2.poo.exception.ExistingPlateException;
import br.ucsal.atividade2.poo.exception.InvalidValueException;
import br.ucsal.atividade2.poo.exception.ListEmptyException;
import br.ucsal.atividade2.poo.utility.UtilityMethods;

public class Locadora {
	private static LocadoraBO locadoraBO = new LocadoraBO();
	private static UtilityMethods utility = new UtilityMethods();

	public static void cadastrarOnibus() {
		String placa = utility.getString("Informe a placa do �nibus: ");
		String modelo = utility.getString("Informe o modelo do �nibus: ");
		Integer anoFabricacao = utility.getInteger("Informe o ano de fabrica��o do �nibus: ");
		double valor = utility.getDouble("Informe o valor do �nibus: ");
		Integer qtdMax = utility.getInteger("Informe a quantidade m�xima de passageiros no �nibus: ");

		Onibus onibus = new Onibus(placa, modelo, anoFabricacao, valor, qtdMax);

		while (true) {
			try {
				locadoraBO.cadastrarOnibus(onibus);
				return;
			} catch (ExistingPlateException e) {
				System.out.println(e.getMessage());
				return;
			} catch (InvalidValueException e) {
				System.out.println(e.getMessage());
				return;
			}
		}
	}

	public static void cadastrarCaminhao() {
		String placa = utility.getString("Informe a placa do caminh�os: ");
		String modelo = utility.getString("Informe o modelo do caminh�o: ");
		Integer anoFabricacao = utility.getInteger("Informe o ano de fabrica��o do caminh�o: ");
		Double valor = utility.getDouble("Informe o valor do caminh�o: ");
		Integer qtdEixos = utility.getInteger("Informe a quantidade de eixos do caminh�o");
		Integer capCarga = utility.getInteger("Informe a capacidade de carga do caminh�o");

		Caminhao caminhao = new Caminhao(placa, modelo, anoFabricacao, valor, qtdEixos, capCarga);

		while (true) {
			try {
				locadoraBO.cadastrarCaminhao(caminhao);
				return;
			} catch (ExistingPlateException e) {
				System.out.println(e.getMessage());
				return;
			} catch (InvalidValueException e) {
				System.out.println(e.getMessage());
				return;
			}
		}
	}

	public static void listarVeiculosOrdenadoPorValor() {
		List<Veiculo> listarPorValor;
		try {
			listarPorValor = locadoraBO.listarVeiculosOrdenadoPorValor();
			for (Veiculo veiculo : listarPorValor) {
				System.out.println(veiculo);
			}
		} catch (ListEmptyException e) {
			System.out.println(e.getMessage());
		}
		System.out.println();
	}

	public static void listarVeiculosOrdenadoPorPlaca() {
		List<Veiculo> listarPorPlaca;
		try {
			listarPorPlaca = locadoraBO.listarVeiculosOrdenadoPorPlaca();
			for (Veiculo veiculo : listarPorPlaca) {
				System.out.println(veiculo);
			}
		} catch (ListEmptyException e) {
			System.out.println(e.getMessage());
		}

		System.out.println();
	}

	public static void listarModelos() {
		Set<String> listarPorModelo;
		try {
			listarPorModelo = locadoraBO.listarModelo();
			System.out.print("Modelos: [");
			for (String modelo : listarPorModelo) {
				if (listarPorModelo == null) {
					System.out.println(".");
				}
				System.out.print(modelo.toUpperCase() + " ");
			}
			System.out.print("]");
		} catch (ListEmptyException e) {
			System.out.println(e.getMessage());
		}

		System.out.println();
	}
}
