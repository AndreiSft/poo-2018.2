package br.ucsal.atividade2.poo.tui;

import br.ucsal.atividade2.poo.utility.UtilityMethods;

public class LocadoraTui {

	private static UtilityMethods utility = new UtilityMethods();

	public static void main(String[] args) {
		System.out.println("Bem-vindo\n\nO que deseja ?\n");
		String option = utility.getString("[Cadastar] | [Listar] | [Sair]");
		menu(option);
	}

	public static void menu(String option) {
		while (true) {
			if (option.equalsIgnoreCase("cadastrar")) {
				cadastrar();
			} else if (option.equalsIgnoreCase("listar")) {
				listar();
			} else if (option.equalsIgnoreCase("sair")) {
				System.exit(0);
			} else {
				System.out.println("Op��o inv�lida");
				goMenu();
			}
		}
	}

	private static void goMenu() {
		String option = utility.getString("Deseja ir para o menu ?");
		if (option.equalsIgnoreCase("sim")) {
			option = utility.getString("O que deseja ?\n\n[Cadastar] | [Listar] | [Sair]");
			menu(option);
		} else if (option.equalsIgnoreCase("n�o") || option.equalsIgnoreCase("nao")) {
			System.out.println("Thank you!");
			System.exit(0);
		}
	}
		
	

	private static void listar() {
		String optionTwo = " ";
		while (true) {
			System.out.println("Deseja listar por:\n ");
			optionTwo = utility.getString("[Valor] - [Placa] - [Modelo]");
			if (optionTwo.equalsIgnoreCase("valor")) {
				Locadora.listarVeiculosOrdenadoPorValor();
				loopListagem();
				return;
			} else if (optionTwo.equalsIgnoreCase("placa")) {
				Locadora.listarVeiculosOrdenadoPorPlaca();
				loopListagem();
				return;
			} else if (optionTwo.equalsIgnoreCase("modelo")) {
				Locadora.listarModelos();
				loopListagem();
				return;
			} else {
				System.out.println("Op��o inv�lida");
				return;
			}
		}
	}

	private static void cadastrar() {
		String optionTwo = " ";
		while (true) {
			System.out.println("Deseja cadastrar um:\n ");
			optionTwo = utility.getString("[�nibus] ou [Caminh�o]");
			if (optionTwo.equalsIgnoreCase("onibus") || optionTwo.equalsIgnoreCase("�nibus")) {
				Locadora.cadastrarOnibus();
				loopCadastro();
				return;
			} else if (optionTwo.equalsIgnoreCase("caminhao") || optionTwo.equalsIgnoreCase("caminh�o")) {
				Locadora.cadastrarCaminhao();
				loopCadastro();
				return;
			} else {
				System.out.println("Op��o inv�lida");
				return;
			}
		}
	}
	
	private static void loopListagem() {
		String option = utility.getString("Deseja continuar Listando ?");
		if (option.equalsIgnoreCase("sim")) {
			listar();
		} else if (option.equalsIgnoreCase("nao") || option.equalsIgnoreCase("n�o")) {
			option = utility.getString("Deseja voltar para o menu ?");
			if (option.equalsIgnoreCase("sim")) {
				option = utility.getString("O que deseja [Cadastrar]  -  [Listar]  -  [Sair] ?");
				menu(option);
			} else {
				System.exit(0);
			}

		} else {
			System.out.println("Op��o inv�lida");
		}
	}
	
	private static void loopCadastro() {
		String option = utility.getString("Deseja continuar adicionando ?");
		if (option.equalsIgnoreCase("sim")) {
			cadastrar();
		} else if (option.equalsIgnoreCase("nao") || option.equalsIgnoreCase("n�o")) {
			option = utility.getString("Deseja voltar para o menu ?");
			if (option.equalsIgnoreCase("sim")) {
				option = utility.getString("O que deseja [Cadastrar] ou [Listar] ?");
				menu(option);
			} else {
				System.exit(0);
			}

		} else {
			System.out.println("Op��o inv�lida");
		}
	}
}
