package br.ucsal.atividade2.poo.exception;

public class ExistingPlateException extends Exception {

	private static final long serialVersionUID = 1L;

	public ExistingPlateException(String message) {
		super(message);
	}
}
