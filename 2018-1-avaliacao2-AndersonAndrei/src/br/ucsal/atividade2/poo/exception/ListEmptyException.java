package br.ucsal.atividade2.poo.exception;

public class ListEmptyException extends Exception{

	private static final long serialVersionUID = 1L;
	
	public ListEmptyException(String message) {
		super(message);
	}
}
