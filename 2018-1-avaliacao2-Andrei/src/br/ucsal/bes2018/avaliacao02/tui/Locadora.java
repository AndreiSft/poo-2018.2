package br.ucsal.bes2018.avaliacao02.tui;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import br.ucsal.bes2018.avaliacao02.domain.Caminhao;
import br.ucsal.bes2018.avaliacao02.domain.Onibus;
import br.ucsal.bes2018.avaliacao02.domain.Veiculo;
import br.ucsal.bes2018.avaliacao02.exception.InvalidValueException;

public class Locadora {

	private static List<Veiculo> veiculos = new ArrayList<>();

	public void cadastrarOnibus(String placa, String modelo, Integer anoFabricacao, Double valor, Integer qtdMax) {
		try {
			Onibus onibus = new Onibus(placa, modelo, anoFabricacao, valor, qtdMax);
			veiculos.add(onibus);
		} catch (InvalidValueException e) {
			System.out.println(e.getMessage());
		}
	}

	public void cadastrarCaminhao(String placa, String modelo, Integer anoFabricacao, Double valor, Integer qtdEixos,
			Integer qtdMaxCarga) {
		Caminhao caminhao;
		try {
			caminhao = new Caminhao(placa, modelo, anoFabricacao, valor, qtdEixos, qtdMaxCarga);
			veiculos.add(caminhao);
		} catch (InvalidValueException e) {
			System.out.println(e.getMessage());
		}
	}

	public void listarVeiculosOrdenadoPorValor() {
		forValue();
		System.out.println("Ordena��o por valor: ");
		for (Veiculo veiculo : veiculos) {
			System.out.print("Placa: " + veiculo.getPlaca() + "\nValor: " + veiculo.getValor() + "/nCusto de Loca��o" + veiculo.custoLocacao());
		}
	}

	public void listarVeiculosOrdenadoPorPlaca() {
		Collections.sort(veiculos);
		System.out.println("Ordena��o por placa: ");
		for (Veiculo veiculo : veiculos) {
			System.out.print(veiculo + " ");
		}
	}

	public void listarModelos() {
		Set<String> modelos = new HashSet();
		for (Veiculo veiculo : veiculos) {
			modelos.add(veiculo.getModelo());
		}
		System.out.println("Modelos cadastrados: ");
		for (String modelo : modelos) {
			System.out.print(modelo + " ");
		}
	}

	private void forValue() {
		Collections.sort(veiculos, new Comparator<Veiculo>() {
			@Override
			public int compare(Veiculo o1, Veiculo o2) {
				return o1.getValor().compareTo(o2.getValor());
			}
		});
	}
}
