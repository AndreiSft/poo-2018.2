package br.ucsal.bes2018.avaliacao02.exception;

public class InvalidValueException extends Exception{

	private static final long serialVersionUID = 1L;

	public InvalidValueException(String message) {
		super(message);
	}
}
