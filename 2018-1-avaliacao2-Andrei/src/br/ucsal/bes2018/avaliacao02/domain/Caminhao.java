package br.ucsal.bes2018.avaliacao02.domain;

import br.ucsal.bes2018.avaliacao02.exception.InvalidValueException;

public class Caminhao extends Veiculo{
	
	private Integer qtdEixos;
	private Integer qtdMaxCarga;
	
	public Caminhao(String placa, String modelo, Integer anoFabricacao, Double valor, Integer qtdEixos,
			Integer qtdMaxCarga) throws InvalidValueException {
		super(placa, modelo, anoFabricacao, valor);
		this.qtdEixos = qtdEixos;
		this.qtdMaxCarga = qtdMaxCarga;
	}
		
	@Override
	public Double custoLocacao() {
		return 8d*qtdMaxCarga;
	}
	
	public Integer getQtdEixos() {
		return qtdEixos;
	}

	public void setQtdEixos(Integer qtdEixos) {
		this.qtdEixos = qtdEixos;
	}

	public Integer getQtdMaxCarga() {
		return qtdMaxCarga;
	}

	public void setQtdMaxCarga(Integer qtdMaxCarga) {
		this.qtdMaxCarga = qtdMaxCarga;
	}

	@Override
	public String toString() {
		return super.toString() + "qtdEixos=" + qtdEixos + ", qtdMaxCarga=" + qtdMaxCarga + "]";
	}



}
