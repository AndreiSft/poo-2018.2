package br.ucsal.bes2018.avaliacao02.domain;

import br.ucsal.bes2018.avaliacao02.exception.InvalidValueException;

public abstract class Veiculo implements Comparable<Veiculo>{
	private String placa;
	private String modelo;
	private Integer anoFabricacao;
	private Double valor;

	public Veiculo(String placa, String modelo, Integer anoFabricacao, Double valor) throws InvalidValueException {
		this.placa = placa;
		this.modelo = modelo;
		this.anoFabricacao = anoFabricacao;
		setValor(valor);
	}
	
	public abstract Double custoLocacao();
	
	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public Integer getAnoFabricacao() {
		return anoFabricacao;
	}

	public void setAnoFabricacao(Integer anoFabricacao) {
		this.anoFabricacao = anoFabricacao;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) throws InvalidValueException {
		validate(valor);
		this.valor = valor;
	}

	private void validate(Double valor) throws InvalidValueException {
		if (valor <= 0) {
			throw new InvalidValueException("Valor n�o permitido");
		}
	}

	@Override
	public String toString() {
		return "Veiculo [placa=" + placa + ", modelo=" + modelo + ", anoFabricacao=" + anoFabricacao + ", valor="
				+ valor;
	}
	
	@Override
	public int compareTo(Veiculo arg0) {
		return placa.compareToIgnoreCase(arg0.placa);
	}
}
