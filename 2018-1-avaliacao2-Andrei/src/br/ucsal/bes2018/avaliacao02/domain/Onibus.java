package br.ucsal.bes2018.avaliacao02.domain;

import br.ucsal.bes2018.avaliacao02.exception.InvalidValueException;

public class Onibus extends Veiculo{
	
	private Integer qtdMax;
	
	public Onibus(String placa, String modelo, Integer anoFabricacao, Double valor, Integer qtdMax)
			throws InvalidValueException {
		super(placa, modelo, anoFabricacao, valor);
		this.qtdMax = qtdMax;
	}
		
	@Override
	public Double custoLocacao() {
		
		return 10d*qtdMax;
	}
	
	public Integer getQtdMax() {
		return qtdMax;
	}

	public void setQtdMax(Integer qtdMax) {
		this.qtdMax = qtdMax;
	}

	@Override
	public String toString() {
		return super.toString() + "qtdMax=" + qtdMax + "]";
	}

	
}
