package br.ucsal.bes20182.poo.domain;

import br.ucsal.bes20182.poo.exception.ValorImovelInvalidoException;

public class Casa extends Imovel{
	private Double areaTerreno;
	private Double areaConstruida;
	
	public Casa(Integer codigo, String endereco, String bairro, Double valor, Double areaTerreno, Double areaConstruida)
			throws ValorImovelInvalidoException {
		super(codigo, endereco, bairro, valor);
		this.areaTerreno = areaTerreno;
		this.areaConstruida = areaConstruida;
	}
	
	@Override
	public Double calculoImposto() {
		return 120d * areaConstruida + 15d * areaTerreno;
	}
	
	public Double getAreaTerreno() {
		return areaTerreno;
	}

	public void setAreaTerreno(Double areaTerreno) {
		this.areaTerreno = areaTerreno;
	}

	public Double getAreaConstruida() {
		return areaConstruida;
	}

	public void setAreaConstruida(Double areaConstruida) {
		this.areaConstruida = areaConstruida;
	}

	@Override
	public String toString() {
		return "Casa " + super.toString() +" areaTerreno=" + areaTerreno + ", areaConstruida=" + areaConstruida + "]";
	}
	
}
