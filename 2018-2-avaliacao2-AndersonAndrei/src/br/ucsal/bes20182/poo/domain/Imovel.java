package br.ucsal.bes20182.poo.domain;

import br.ucsal.bes20182.poo.exception.ValorImovelInvalidoException;

public abstract class Imovel implements Comparable<Imovel> {
	private Integer codigo;
	private String endereco;
	private String bairro;
	private Double valor;

	public Imovel(Integer codigo, String endereco, String bairro, Double valor) throws ValorImovelInvalidoException {
		super();
		this.codigo = codigo;
		this.endereco = endereco;
		this.bairro = bairro;
		setValor(valor);
	}
	
	public abstract Double calculoImposto();
	
	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) throws ValorImovelInvalidoException {
		validate(valor);
		this.valor = valor;
	}

	@Override
	public int compareTo(Imovel arg0) {
		return codigo.compareTo(arg0.codigo);
	}

	@Override
	public String toString() {
		return "[codigo=" + codigo + ", endereco=" + endereco + ", bairro=" + bairro + ", valor=" + valor;
	}

	private void validate(Double valor) throws ValorImovelInvalidoException {
		if (valor <= 0) {
			throw new ValorImovelInvalidoException("Valor inv�lido");
		}
	}

}
