package br.ucsal.bes20182.poo.domain;

import br.ucsal.bes20182.poo.exception.ValorImovelInvalidoException;

public class Apartamento extends Imovel{
	private Double areaFracao;
	private Double areaPrivativa;

	public Apartamento(Integer codigo, String endereco, String bairro, Double valor, Double areaFracao,
			Double areaPrivativa) throws ValorImovelInvalidoException {
		super(codigo, endereco, bairro, valor);
		this.areaFracao = areaFracao;
		this.areaPrivativa = areaPrivativa;
	}
	
	@Override
	public Double calculoImposto() {
		return 100d * areaPrivativa + 30d * areaFracao;
	}
	
	public Double getAreaFracao() {
		return areaFracao;
	}

	public void setAreaFracao(Double areaFracao) {
		this.areaFracao = areaFracao;
	}

	public Double getAreaPrivativa() {
		return areaPrivativa;
	}

	public void setAreaPrivativa(Double areaPrivativa) {
		this.areaPrivativa = areaPrivativa;
	}

	@Override
	public String toString() {
		return "Apartamento "+ super.toString() + " areaFracao=" + areaFracao + ", areaPrivativa=" + areaPrivativa + "]";
	}
	
}
