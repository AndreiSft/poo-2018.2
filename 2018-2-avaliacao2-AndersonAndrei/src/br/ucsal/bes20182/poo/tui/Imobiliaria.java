package br.ucsal.bes20182.poo.tui;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import br.ucsal.bes20182.poo.domain.Casa;
import br.ucsal.bes20182.poo.domain.Imovel;
import br.ucsal.bes20182.poo.exception.ValorImovelInvalidoException;

public class Imobiliaria {
	private static List<Imovel> imoveis = new ArrayList<>();

	public static void cadastrarCasa(Integer codigo, String endereco, String bairro, Double valor, Double areaTerreno,
			Double areaConstruida) {
		try {
			Casa casa = new Casa(codigo, endereco, bairro, valor, areaTerreno, areaConstruida);
			imoveis.add(casa);
		} catch (ValorImovelInvalidoException e) {
			System.out.println(e.getMessage());
		}
	}

	public static void listarImoveisPorCodigo() {
		Collections.sort(imoveis);

		for (Imovel imovel : imoveis) {
			System.out.println(imovel.getCodigo() + " " +  imovel.getEndereco()  + " " +  imovel.getBairro());
		}
	}

	public static void listarImoveisPorBairroValor() {
		Collections.sort(imoveis, new Comparator<Imovel>() {
			@Override
			public int compare(Imovel o1, Imovel o2) {
				return o1.getBairro().compareTo(o2.getBairro());
			}
		});
		
		Collections.sort(imoveis, new Comparator<Imovel>() {//Tentantiva de listar por valor dentro de cada bairro
			@Override
			public int compare(Imovel o1, Imovel o2) {
				return o1.getValor().compareTo(o2.getValor());
			}
		});
		
		
		for (Imovel imovel : imoveis) {	
			System.out.println(imovel.getCodigo() + " " +  imovel.getEndereco() + " " +imovel.calculoImposto());
		}
	}

	public static void listarBairros() {
		Set<String> bairros = new HashSet<>();
		for (Imovel imovel : imoveis) {
			bairros.add(imovel.getBairro());
		}
		System.out.print("Bairros: [ ");
		for (String bairro : bairros) {
			System.out.print(bairro + " ");
		}
		System.out.println("]");
	}
}
