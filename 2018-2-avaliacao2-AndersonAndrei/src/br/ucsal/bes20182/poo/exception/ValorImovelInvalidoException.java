package br.ucsal.bes20182.poo.exception;

public class ValorImovelInvalidoException extends Exception{

	private static final long serialVersionUID = 1L;
	
	public ValorImovelInvalidoException(String message) {
		super(message);
	}
}
