package br.ucsal.domain;

public class Caminhoes extends Veiculo {

	private Integer qtdEixos;
	private TipoCargaEnum tipoCarga;
	private TipoVeiculoEnum tipoVeiculo = TipoVeiculoEnum.CAMINHAO;
	
	public Caminhoes(String placa, Integer veiculo, double valor, Integer qtdEixos, TipoCargaEnum tipoCarga) {
		super(placa, veiculo, valor);
		this.qtdEixos = qtdEixos;
		this.tipoCarga = tipoCarga;
	}

	@Override
	public String toString() {
		return "Veiculo [tipoVeiculo=" + tipoVeiculo + ", placa=" + getPlaca() + ", veiculo=" + getVeiculo() + ", valor=" + getValor() + ", qtdEixos=" + qtdEixos +  ", tipoCarga=" + tipoCarga + "]";
	}
	
}
