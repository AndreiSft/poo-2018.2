package br.ucsal.domain;

import java.util.ArrayList;
import java.util.List;

public abstract class Veiculo implements Comparable<Veiculo> {
	private String placa;
	private Integer veiculo;
	private double valor;
	private List<Veiculo> veiculos = new ArrayList<>();

	public Veiculo(String placa, Integer veiculo, double valor) {
		super();
		this.placa = placa;
		this.veiculo = veiculo;
		this.valor = valor;
	}

	public Integer getVeiculo() {
		return veiculo;
	}
	
	public String getPlaca() {
		return placa;
	}

	public double getValor() {
		return valor;
	}
	
	public int compareTo(Veiculo veiculo) {
		return this.placa.compareToIgnoreCase(veiculo.placa);
		/*
		 * if (this.placa.compareToIgnoreCase(veiculo.placa) == -1) { return -1; }
		 * 
		 * if (this.placa.compareToIgnoreCase(veiculo.placa) == 1) { return 1; } return
		 * 0;
		 */ }

	@Override
	public String toString() {
		return "Veiculo [placa=" + placa + ", veiculo=" + veiculo + ", valor=" + valor + "]";
	}
}
