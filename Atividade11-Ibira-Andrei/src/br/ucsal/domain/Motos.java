package br.ucsal.domain;

public class Motos extends Veiculo {
	private TipoMotoEnum tipoMoto;
	private TipoVeiculoEnum tipoVeiculo = TipoVeiculoEnum.MOTO;
	
	public Motos(String placa, Integer veiculo, double valor, TipoMotoEnum tipoMoto) {
		super(placa, veiculo, valor);
		this.tipoMoto = tipoMoto;
	}
	
	@Override
	public String toString() {
		return "Veiculo [tipoVeiculo=" + tipoVeiculo + ", placa=" + getPlaca() + ", veiculo=" + getVeiculo() + ", valor=" + getValor() + ", tipoMoto=" + tipoMoto + "]";
	}
	
}
