package br.ucsal.domain;

public class Carros extends Veiculo {

	private Integer qtdPortas;
	private TipoVeiculoEnum tipoVeiculo = TipoVeiculoEnum.CARRO;
	
	public Carros(String placa, Integer veiculo, double valor, Integer qtdPortas) {
		super(placa, veiculo, valor);
		this.qtdPortas = qtdPortas;
	}
	@Override
	public String toString() {
		return "Veiculo [tipoVeiculo=" + tipoVeiculo + ", placa=" + getPlaca() + ", veiculo=" + getVeiculo() + ", valor=" + getValor() + ", qtdPortas=" + qtdPortas + "]";
	}
}