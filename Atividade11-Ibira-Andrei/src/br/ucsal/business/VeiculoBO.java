package br.ucsal.business;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import br.ucsal.domain.TipoVeiculoEnum;
import br.ucsal.domain.Veiculo;

public class VeiculoBO {
	private static List<Veiculo> veiculos = new ArrayList<>();

	public static void incluir(Veiculo veiculo) {
		veiculos.add(veiculo);
	}

	public static void listar() {
		Collections.sort(veiculos);
		for (Veiculo veiculo : veiculos) {		
			System.out.println(veiculo);
			System.out.println();
		}
	}

}
