package br.ucsal.tui;

import br.ucsal.business.VeiculoBO;
import br.ucsal.domain.Caminhoes;
import br.ucsal.domain.Carros;
import br.ucsal.domain.Motos;
import br.ucsal.domain.TipoCargaEnum;
import br.ucsal.domain.TipoMotoEnum;

public class VeiculoTui {
	
	public static void main(String[] args) {
		Carros carro = new Carros("abc123", 2010, 100d, 4);
		Carros carro1 = new Carros("def456", 2009, 100d, 2);
		Carros carro2 = new Carros("ghj789", 2010, 100d, 4);
		
		VeiculoBO.incluir(carro);
		VeiculoBO.incluir(carro1);
		VeiculoBO.incluir(carro2);
		
		Motos moto = new Motos("def456", 2009, 100d, TipoMotoEnum.CAMPO);
		Motos moto1 = new Motos("ghj789", 2010, 100d, TipoMotoEnum.CAMPO);
		Motos moto2 = new Motos("abc123", 2010, 100d, TipoMotoEnum.CAMPO);
		
		VeiculoBO.incluir(moto);
		VeiculoBO.incluir(moto1);
		VeiculoBO.incluir(moto2);
		
		Caminhoes caminhoes = new Caminhoes("ghj789", 2009, 100d, 3, TipoCargaEnum.GAS);
		Caminhoes caminhoes1 = new Caminhoes("abc123", 2009, 100d, 3, TipoCargaEnum.GAS);
		Caminhoes caminhoes2 = new Caminhoes("def456", 2009, 100d, 3, TipoCargaEnum.GAS);
		
		VeiculoBO.incluir(caminhoes);
		VeiculoBO.incluir(caminhoes1);
		VeiculoBO.incluir(caminhoes2);
		
		VeiculoBO.listar();
	}
	
}
