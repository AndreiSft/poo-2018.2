package br.ucsal.bes20181.poo.avaliacao1.tui;

import br.ucsal.bes20181.poo.avaliacao1.domain.Contrato;
import br.ucsal.bes20181.poo.avaliacao1.domain.TipoVeiculoEnum;
import br.ucsal.bes20181.poo.avaliacao1.domain.Veiculo;

public class ContratoTui {

	public static void main(String[] args) {
      Veiculo veiculo1 = new Veiculo("ABC-123", 2001, TipoVeiculoEnum.BASICO);
      Veiculo veiculo2 = new Veiculo("ABC-321", 2004, TipoVeiculoEnum.LUXO);
      Veiculo veiculo3 = new Veiculo("ABC-456", 2005, TipoVeiculoEnum.INTERMEDIARIO);

      Contrato contrato1 = new Contrato("Andrei", "rua-02");
      Contrato contrato2 = new Contrato("Andreia", "rua-03");
      
      contrato1.addVeiculo(veiculo1);
      contrato1.addVeiculo(veiculo2);
      
      listarVeiculosContrato(contrato1);
      
      contrato1.removerVeiculo("ABC-123");
      
      listarVeiculosContrato(contrato1);
      
	}
	private static void listarVeiculosContrato(Contrato contrato) {
		System.out.println("Veiculos do contratante " + contrato.getNome());
		for (Veiculo veiculo : contrato.consultarVeiculos()){
			System.out.println(veiculo);
		}
	}

}
