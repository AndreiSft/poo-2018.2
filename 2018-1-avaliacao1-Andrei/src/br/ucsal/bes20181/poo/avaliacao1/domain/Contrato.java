package br.ucsal.bes20181.poo.avaliacao1.domain;

import java.util.ArrayList;
import java.util.List;

public class Contrato {
	private static Integer contador = 0;
	
	private Integer numContrato;
	private String nome;
	private String endereco;
	private List<Veiculo> veiculos = new ArrayList<>();
	private Double vlrContrato = 0d;

	public Contrato(String nome, String endereco) {
		this.nome = nome;
		this.endereco = endereco;
		gerarNumero();
	}

	public String getNome() {
		return nome;
	}

	public String getEndereco() {
		return endereco;
	}

	public Double getVlrContrato() {
		return vlrContrato;
	}

	public boolean addVeiculo(Veiculo veiculo) {
		if (consultarVeiculo(veiculo.getPlaca()) != null) {
			return false;
		}

		veiculos.add(veiculo);
		adicionarValorContrato(veiculo.getTipo());
		return true;
	}

	public boolean removerVeiculo(String placa) {
		Veiculo veiculo = consultarVeiculo(placa);
		if (veiculo == null) {
			return false;
		}

		veiculos.remove(veiculo);
		removerValorContrato(veiculo.getTipo());
		return true;
	}

	public List<Veiculo> consultarVeiculos() {

		return veiculos;
	}

	public List<Veiculo> consultarVeiculos(String tipoVeiculo) {
		TipoVeiculoEnum tipo = TipoVeiculoEnum.valueOf(tipoVeiculo.toUpperCase());
		List<Veiculo> veiculosSelecionados = new ArrayList<>();
		for (Veiculo veiculo : veiculos) {
			if (veiculo.getTipo().equals(tipo)) {
				veiculosSelecionados.add(veiculo);
			}
		}
		return veiculosSelecionados;
	}

	public Veiculo consultarVeiculo(String placa) {
		for (Veiculo veiculo : veiculos) {
			if (veiculo.getPlaca().equalsIgnoreCase(placa)) {
				return veiculo;
			}
		}
		return null;
	}

	private void adicionarValorContrato(TipoVeiculoEnum tipo) {
		vlrContrato += tipo.getValorDiaria();
	}

	private void removerValorContrato(TipoVeiculoEnum tipo) {
		vlrContrato -= tipo.getValorDiaria();
	}

	private void gerarNumero() {
		contador++;
		this.numContrato = contador;
	}

	// public List<Veiculo> consultarVeiculos(TipoVeiculoEnum tipoVeiculo){
	// List<Veiculo> veiculoSelecionados = new ArrayList();
	// for (Veiculo veiculo : veiculos) {
	// if (veiculo.getTipo().equals(tipoVeiculo)) {
	// veiculoSelecionados.add(veiculo);
	// }
	// }
	// return veiculoSelecionados;
	// } recebendo um enum como parametro
}
