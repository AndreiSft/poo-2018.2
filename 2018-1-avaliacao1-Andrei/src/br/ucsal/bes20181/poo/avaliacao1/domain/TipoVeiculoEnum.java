package br.ucsal.bes20181.poo.avaliacao1.domain;

public enum TipoVeiculoEnum {
	BASICO(100.45), INTERMEDIARIO(130.10), LUXO(156d);
	private Double valorDiaria;

	private TipoVeiculoEnum(Double valorDiaria) {
		this.valorDiaria = valorDiaria;
	}
	
	public Double getValorDiaria() {
		return valorDiaria;
	}
}
